﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using Terraria;
using Terraria.ModLoader;
using Terraria.ModLoader.IO;
using Terraria.GameContent.Events;
using Terraria.ID;
using Terraria.IO;
using Terraria.UI;

namespace VisualRadar {
	public class MPlayer : ModPlayer {
		float[] scale = new float[Main.npc.Length];
		Texture2D[] texture = new Texture2D[Main.npc.Length];
		static float M1MinDist = 32f;
		static float M2DistMult = 1f/3f;
		static float ScaleMax = 1f;
		static float CScaleMax = 2f/3f;
		static float ScaleDiff = 1f/30f;
		public override void DrawEffects(PlayerDrawInfo drawInfo, ref float r, ref float g, ref float b, ref float a, ref bool fullBright) {
			try {
				if (Config.Client.Method == 0) { return; }
				if (Main.netMode != 2 && !Main.gameMenu && player.whoAmI == Main.myPlayer) {
					int npcIndex = 0;
					for (int i = 0; i < Main.npc.Length; i++) {
						if (scale[i] == null) { scale[i] = 0f; }
						NPC npc = Main.npc[i];
						bool isChild = Checks.IsChild(npc);
						bool isBoss = Checks.IsBoss(npc);
						bool isBossPiece = Checks.IsBossPiece(npc);
						bool isRare = npc.rarity > 0;
						bool isNormal = (!isChild && Checks.IsValidNPC(npc)) || (isChild && Checks.IsValidNPC(Main.npc[npc.realLife]));
						float NPCDistance = Vector2.Distance(player.Center, npc.Center);
						if (npc.type != 0 && npc.active && npc.life > 0 && ((isBoss && !isBossPiece) || NPCDistance < 1300)) {
							if (isBoss && ((player.accThirdEye && !player.hideInfo[5]) || (player.accCritterGuide && !player.hideInfo[11]))) {
								if (npc.GetBossHeadTextureIndex() >= 0 && npc.GetBossHeadTextureIndex() < Main.npcHeadBossTexture.Length) {
									texture[i] = Main.npcHeadBossTexture[npc.GetBossHeadTextureIndex()];
								}
								else { texture[i] = Main.instance.infoIconTexture[9]; }
								if (scale[i] < ScaleMax) { scale[i] += ScaleDiff; }
							}
							else if (isBossPiece && ((player.accThirdEye && !player.hideInfo[5]) || (player.accCritterGuide && !player.hideInfo[11]))) {
								texture[i] = Main.instance.infoIconTexture[9];
								if (scale[i] < ScaleMax) { scale[i] += ScaleDiff; }
							}
							else if (isRare && (player.accCritterGuide && !player.hideInfo[11])) {
								texture[i] = Main.instance.infoIconTexture[11];
								if (!isChild && scale[i] < ScaleMax) { scale[i] += ScaleDiff; }
								else if (isChild && scale[i] < CScaleMax) { scale[i] += ScaleDiff; }
							}
							else if (isNormal && player.accThirdEye && !player.hideInfo[5]) {
								texture[i] = Main.instance.infoIconTexture[5];
								if (!isChild && scale[i] < ScaleMax) { scale[i] += ScaleDiff; }
								else if (isChild && scale[i] < CScaleMax) { scale[i] += ScaleDiff; }
							}
							else if (scale[i] > 0f) { scale[i] -= ScaleDiff; }
						}
						else if (scale[i] > 0f) { scale[i] -= ScaleDiff; }
						if (texture[i] != null && scale[i] > 0f) {
							Vector2 IconPosition = new Vector2(player.Center.X - Main.screenPosition.X, player.Center.Y - Main.screenPosition.Y);
							float IconDistance = 0;
							float RelativeAngle = (float)Math.Atan2(player.Center.Y - npc.Center.Y, player.Center.X - npc.Center.X);
							Vector2 TexturePos = new Vector2();
							if (Config.Client.Method == 1) {
								IconDistance = M1MinDist;
								for (int x = 0; x < i; x++) {
									IconDistance += (texture[i].Width * scale[x] * 2);
								}
								TexturePos.X = (float)(Math.Cos(RelativeAngle) * -(IconDistance) + IconPosition.X);
								TexturePos.Y = (float)(Math.Sin(RelativeAngle) * -(IconDistance) + IconPosition.Y);
							}
							else if (Config.Client.Method == 2) {
								IconDistance = NPCDistance * M2DistMult;
								TexturePos.X = MathHelper.Clamp((float)(Math.Cos(RelativeAngle) * -(IconDistance) + IconPosition.X), 32, Main.PendingResolutionWidth-32);
								TexturePos.Y = MathHelper.Clamp((float)(Math.Sin(RelativeAngle) * -(IconDistance) + IconPosition.Y), 32, Main.PendingResolutionHeight-32);
							}
							if (IconDistance < Math.Max(Main.PendingResolutionWidth,Main.PendingResolutionHeight)) {
								Main.spriteBatch.Draw(texture[i], TexturePos, new Rectangle?(new Rectangle(0, 0, texture[i].Width, texture[i].Height)), new Color(1f, 1f, 1f, 1f) * scale[i] * Config.Client.Alpha, 0f, new Vector2(texture[i].Width/2f,texture[i].Height/2f), scale[i], SpriteEffects.None, 0f);
							}
						}
					}
				}
			}
			catch {}
		}
	}
}
