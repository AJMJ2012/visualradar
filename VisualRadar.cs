﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System;
using Terraria.GameContent.Events;
using Terraria.ID;
using Terraria.IO;
using Terraria.ModLoader.IO;
using Terraria.ModLoader;
using Terraria.UI;
using Terraria;

namespace VisualRadar {
	public class VisualRadar : Mod {
		public VisualRadar() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}

		bool LoadedFKTModSettings = false;
		public override void Load() {
			if (Main.netMode == 2) { return; }
			Config.ReadConfig();
			LoadedFKTModSettings = ModLoader.GetMod("FKTModSettings") != null;
			if (LoadedFKTModSettings) {
				try { LoadModSettings(); }
				catch (Exception e) {
					DALib.Logger.ErrorLog("Unable to Load Mod Settings", Config.modPrefix);
					DALib.Logger.ErrorLog(e, Config.modPrefix);
				}
			}
		}

		public override void PostUpdateInput() {
			if (LoadedFKTModSettings && !Main.gameMenu && Main.netMode != 2) {
				if (DALib.DALib.tick % 60 == 0) {
					try {
						List<Type> Types = new List<Type>{ typeof(Config.Global) };
						if (Main.netMode != 2) { Types.Add(typeof(Config.Client)); }
						if (Main.netMode != 1) { Types.Add(typeof(Config.Server)); }
						string OldConfig = null;
						foreach (Type type in Types) { OldConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						UpdateModSettings();
						string NewConfig = null;
						foreach (Type type in Types) { NewConfig += JsonConvert.SerializeObject((type.GetFields(BindingFlags.Static | BindingFlags.Public).Select(field => field.GetValue(type)))); }
						if (OldConfig != NewConfig) { Config.SaveConfig(); }
					}
					catch (Exception e) {
						DALib.Logger.ErrorLog("Unable to compare config data", Config.modPrefix);
						DALib.Logger.ErrorLog(e, Config.modPrefix);
					}
				}
			}
		}

		private void LoadModSettings() {
			FKTModSettings.ModSetting setting = FKTModSettings.ModSettingsAPI.CreateModSettingConfig(this);
			setting.EnableAutoConfig();
			setting.AddInt("Method", "Method", 0, 2, false);
			setting.AddComment("How the icons are drawn", 0.8f);
			setting.AddComment("0: Disabled", 0.8f);
			setting.AddComment("1: Icons are evenly spaced out and don't represent the distance of the NPC from you", 0.8f);
			setting.AddComment("2: Icons are spaced out based on the distance of the NPC from you", 0.8f);
			setting.AddFloat("Alpha", "Alpha", 0f, 1f, false);
			setting.AddComment("How transparent the icons are", 0.8f);
			setting.AddComment("0.0: Invisible", 0.8f);
			setting.AddComment("0.5: Half Visible", 0.8f);
			setting.AddComment("1.0: Completely Visible", 0.8f);
		}

		private void UpdateModSettings() {
			FKTModSettings.ModSetting setting;
			if (FKTModSettings.ModSettingsAPI.TryGetModSetting(this, out setting))  {
				setting.Get("Method", ref Config.Client.Method);
				setting.Get("Alpha", ref Config.Client.Alpha);
			}
		}
	}

	public static class Config {
		public static class Global {}
		public static class Client {
			public static int Method = 2;
			public static float Alpha = 1f;
		}
		public static class Server {}

		public static string modName = "VisualRadar";
		public static string modPrefix = "VR";
		private static Preferences Configuration = new Preferences(Path.Combine(Main.SavePath, "Mod Configs/" + modName + ".json"));

		public static void Load() {
			ReadConfig();
		}

		public static void ReadConfig() {
			if (Configuration.Load()) {
				Configuration.Get("Method", ref Client.Method);
				Configuration.Get("Alpha", ref Client.Alpha);
				DALib.Logger.DebugLog("Config Loaded", Config.modPrefix);
			}
			else {
				DALib.Logger.DebugLog("Creating Config", Config.modPrefix);
			}
			SaveConfig();
		}

		public static void ClampConfig() {
			Client.Method = (int)MathHelper.Clamp(Client.Method, 0, 2);
			Client.Alpha = MathHelper.Clamp(Client.Alpha, 0, 1);
		}

		public static void SaveConfig() {
			ClampConfig();
			Configuration.Clear();
			Configuration.Put("Method", Client.Method);
			Configuration.Put("Alpha", Client.Alpha);
			Configuration.Save();
			DALib.Logger.DebugLog("Config Saved", Config.modPrefix);
		}
	}

	public static class Checks {
		public static bool IsBoss(NPC npc) {
			if (DALib.Checks.IsBoss(npc)) { return true; }
			switch (npc.type) {
				case NPCID.EaterofWorldsHead:
				case NPCID.GolemHead:
				case NPCID.QueenBee:
					return true;
			}
			return false;
		}

		public static bool IsBossPiece(NPC npc) {
			if (IsChild(npc) && DALib.Checks.IsBoss(Main.npc[npc.realLife])) { return true; }
			switch (npc.type) {
				case NPCID.Creeper:
				case NPCID.EaterofWorldsBody:
				case NPCID.EaterofWorldsTail:
				case NPCID.Golem:
				case NPCID.GolemFistLeft:
				case NPCID.GolemFistRight:
				case NPCID.MartianSaucer:
				case NPCID.MartianSaucerCannon:
				case NPCID.MartianSaucerTurret:
				case NPCID.MoonLordCore:
				case NPCID.MoonLordHand:
				case NPCID.MoonLordHead:
				case NPCID.MoonLordFreeEye:
				case NPCID.PlanterasHook:
				case NPCID.PlanterasTentacle:
				case NPCID.PrimeCannon:
				case NPCID.PrimeLaser:
				case NPCID.PrimeSaw:
				case NPCID.PrimeVice:
				case NPCID.SkeletronHand:
				case NPCID.SkeletronHead:
				case NPCID.TheDestroyerBody:
				case NPCID.TheDestroyerTail:
				case NPCID.TheHungry:
				case NPCID.TheHungryII:
				case NPCID.WallofFlesh:
				case NPCID.WallofFleshEye:
				case NPCID.Probe:
					return true;
			}
			return false;
		}
		
		public static bool IsChild(NPC npc) {
			return (npc.realLife >= 0 && npc.realLife != npc.whoAmI);
		}
		
		public static bool IsValidNPC(NPC npc) {
			return (!npc.friendly && npc.damage > 0 && npc.lifeMax > 0 && !npc.dontCountMe && !npc.hide && !npc.dontTakeDamage);
		}
	}

	public class Commands : ModCommand {
		public override CommandType Type {
			get { return CommandType.Chat; }
		}
		public override string Command {
			get { return Config.modPrefix.ToLower(); }
		}
		public override string Description {
			get { return mod.Name; }
		}
		public override string Usage {
			get { return Command + " reload"; }
		}
		public override void Action(CommandCaller caller, string input, string[] args) {
			switch (args[0].ToLower()) {
				case "reload":
					DALib.Logger.Log("Config Reloaded", Config.modPrefix);
					Config.ReadConfig();
					return;
			}
		}
	}
}
